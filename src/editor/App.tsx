import Footer from './components/Footer';
import Header from './components/Header';
import FormAddToBlacklist from './forms/FormAddToBlacklist';
import FormCookieUpdate from './forms/FormCookieUpdate';
import ListCookies from './lists/ListCookies';
import Blacklist from "./components/Blacklist";
import { atomIsAsideClosed } from './atoms';
import { useAtom } from 'jotai';

function App() {

  const [IsAsideClosed] = useAtom(atomIsAsideClosed);

  return (
    <>
      <main className='h-100 overflow' style={{ width: IsAsideClosed == 0 ? '100%' : '80%' }}>
        <Header />
        <ListCookies clx={IsAsideClosed == 0 ? 'grid-4' : 'grid-3'} />
        <Footer />
      </main>

      {IsAsideClosed == 1 && <aside className="h-100 bg-black border-left overflow">
        <FormCookieUpdate />
        <FormAddToBlacklist />
        <Blacklist />
      </aside>}
    </>
  )
}

export default App
