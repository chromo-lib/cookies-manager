import { atom } from "jotai";

export const atomCookies = atom<chrome.cookies.Cookie[]>([]);

export const atomBlackListDomains = atom<string[]>([]);

export const atomCookiesLen = atom<number>(0);

// @ts-ignore: Unreachable code error
export const atomSelectedCookie = atom<chrome.cookies.Cookie>({ name: '', domain: '', path: '', secure: false });

export const atomSelectedCookies = atom<number>(0);

export const atomModal = atom<boolean>(true);

export const atomIsAsideClosed = atom<number | string>(localStorage.getItem('aside-status') || 1);
