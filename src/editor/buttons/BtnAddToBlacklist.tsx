import { useAtom } from "jotai";
import { atomSelectedCookies } from "../atoms";
import IconPlus from "../icons/IconPlus";
import BlackListService from "../services/BlackListService";

export default function BtnAddToBlacklist() {
  const [selectedCookies, setSelectedCookies] = useAtom(atomSelectedCookies);

  const onAdd = () => {
    const elements = document.querySelectorAll('input[type="checkbox"]');
    const isChk = [...elements].reduce((a, el: any) => a += el.checked, 0);

    if (isChk > 0 && window.confirm('Do you really want add these domains to blacklist?')) {
      const domains = [];

      for (const el of elements) {
        const element = el as HTMLInputElement;
        if (element.checked === true) {
          const cookie = JSON.parse(element.value) as chrome.cookies.Cookie;
          domains.push(cookie.domain);
          element.checked = false;
        }
      }

      BlackListService.setAll(domains);
      setSelectedCookies(0)
    }
  }

  return <button className={'border-right border-left' + (selectedCookies > 0 ? ' bg-yellow' : '')} onClick={onAdd}
    title="Add Selected Domains To Blacklist">
    <IconPlus clx="mr-1" />({selectedCookies})
  </button>
}
