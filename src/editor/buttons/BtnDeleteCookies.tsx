import { useAtom } from "jotai";
import { atomSelectedCookies } from "../atoms";
import IconTrash from "../icons/IconTrash";

export default function BtnDeleteCookies() {

  const [selectedCookies, setSelectedCookies] = useAtom(atomSelectedCookies);

  const onDelete = () => {
    const elements = document.querySelectorAll('input[type="checkbox"]');
    const isChk = [...elements].reduce((a, el: any) => a += el.checked, 0);
    
    if (isChk > 0 && window.confirm('Do you really want to delete?')) {
      const tempCookies = [];
      
      for (const el of elements) {
        const element = el as HTMLInputElement;
        if (element.checked === true) {
          tempCookies.push(JSON.parse(element.value)!);
          element.checked = false;
        }
      }

      chrome.runtime.sendMessage({ cookies: tempCookies, message: 'delete:cookies', isChrome: navigator.userAgent.indexOf("Chrome") != -1 });
      setSelectedCookies(0);
    }
  }

  return <button className={'border-right border-left' + (selectedCookies > 0 ? ' bg-red' : '')} onClick={onDelete} title="Delete Selected Cookies">
    <IconTrash clx="mr-1" />({selectedCookies})
  </button>
}
