import { useAtom } from 'jotai';
import { useEffect, useRef } from 'react';
import { atomCookies, atomCookiesLen } from '../atoms';
import IconLoad from '../icons/IconLoad';

const chunkLen = 50;

export default function BtnLoadMore() {
  const chunkLenRef = useRef(50);
  const [cookies] = useAtom(atomCookies);
  const [cookiesLen] = useAtom(atomCookiesLen);

  useEffect(() => {
    chunkLenRef.current = chunkLen;
  }, [cookiesLen])


  const onloadMore = () => {
    if (chunkLen <= cookiesLen) {
      chunkLenRef.current += chunkLen
      chrome.runtime.sendMessage({ message: 'get:cookies', chunkLen: chunkLenRef.current });
    }
  }

  return <button onClick={onloadMore} title="Load More" disabled={cookies.length < 50}>
    <IconLoad clx="mr-1" />({cookies.length} / {cookiesLen})
  </button>
}
