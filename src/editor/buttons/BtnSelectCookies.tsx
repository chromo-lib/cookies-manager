import { useAtom } from "jotai";
import { atomSelectedCookies } from "../atoms";
import IconCheck from "../icons/IconCheck";

export default function BtnSelectCookies() {
  const [selectedCookies, setSelectedCookies] = useAtom(atomSelectedCookies);

  const onSelectAll = () => {
    const elements = document.querySelectorAll('input[type="checkbox"]');
    const isc = (elements[0] as HTMLInputElement).checked;

    let counter = 0;

    for (const el of elements) {
      const element = el as HTMLInputElement;
      if (element.checked === false) counter++;
      element.checked = !isc;
    }

    setSelectedCookies(counter);
  }

  return <button className={selectedCookies > 0 ? 'bg-green border-right' : ' border-right'} onClick={onSelectAll} title="(Un) Select All Visible">
    <IconCheck clx="mr-1" />({selectedCookies})
  </button>
}
