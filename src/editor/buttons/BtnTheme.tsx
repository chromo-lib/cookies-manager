import { useCallback } from 'react'
import IconTheme from '../icons/IconTheme';

export default function BtnTheme() {

  const onTheme = useCallback(() => {
    const theme = document.body.dataset.theme === 'dark' ? 'light' : 'dark';
    document.body.dataset.theme = theme;
    localStorage.setItem('theme', theme);
  }, [])

  return <button className='border-right' onClick={onTheme} title="Change Theme"><IconTheme /></button>
}
