import { useAtom } from "jotai";
import { useCallback, useRef } from "react";
import { atomBlackListDomains } from "../atoms";
import FormFilterBlacklis from "../forms/FormFilterBlacklis";
import IconCopy from "../icons/IconCopy";
import IconDownload from "../icons/IconDownload";
import IconTrash from "../icons/IconTrash";
import IconUpload from "../icons/IconUpload";
import ListBlackDomains from "../lists/ListBlackDomains";
import BlackListService from "../services/BlackListService";
import copy from "../utils/copy";

export default function BlackDomains() {
  const [domains, setDomains] = useAtom(atomBlackListDomains);
  const inputRef = useRef<HTMLInputElement>(null)

  const onClear = () => {
    if (window.confirm('Do really want to clear storage?')) {
      chrome.runtime.sendMessage({ message: 'clear:blacklist' }, (response) => {
        setDomains(response)
      });
    }
  }

  const onExport = useCallback(() => {
    BlackListService.getAll().then(dms => {
      const data = JSON.stringify(dms)
      const a = document.createElement("a");
      const file = new Blob([data], { type: 'text/plain' });
      a.href = URL.createObjectURL(file);
      a.download = 'cookies.json';
      a.click();
    });
  }, []);

  const onUpload = useCallback(() => {
    inputRef.current!.click();
  }, []);

  const onchange = (e: any) => {
    if (window.confirm('Do you really want to upload these new domains?')) {
      const files = e.target.files;
      if (files && files[0]) {
        const reader = new FileReader();
        reader.readAsText(files[0], "UTF-8");

        reader.onload = async (evt: any) => {
          const content = evt.target.result.trim();
          let domains = [];

          if (files[0].name.endsWith('.json') && content.startsWith('["')) domains = JSON.parse(content);
          else domains = content.split(/\s+|\r\n|\n/g);

          await BlackListService.setAll(domains.filter((v: string) => v));
        }
      }
    }
  }

  const onCopy = async () => {
    copy(await BlackListService.getAll());
  }

  return (
    <div title="Black List Domains" className="border-bottom p-1">
      <p>Black list domains ({domains.length})</p>
      <FormFilterBlacklis />
      <ListBlackDomains />

      <input type="file" name="file" id="file" ref={inputRef} onChange={onchange} accept=".txt,.json" required hidden />

      <div className="d-flex">
        <button className="w-100 bg-dark" onClick={onUpload} title="Upload"><IconUpload /></button>
        <button className="w-100 bg-dark" onClick={onExport} title="Download"><IconDownload /></button>
        <button className="w-100 bg-dark" onClick={onCopy} title="Copy All"><IconCopy /></button>
        <button className="w-100 bg-dark" onClick={onClear} title="Clear All"><IconTrash /></button>
      </div>
    </div>
  )
}
