import { useAtom } from 'jotai'
import { atomSelectedCookie, atomSelectedCookies } from '../atoms';
import IconCopy from '../icons/IconCopy';
import IconInfo from '../icons/IconInfo';
import IconPlus from '../icons/IconPlus';
import BlackListService from '../services/BlackListService';
import copy from '../utils/copy';

export default function CookieView({ c, index }: { c: chrome.cookies.Cookie, index: number }) {
  const [currentCookie, setCookie] = useAtom(atomSelectedCookie);
  const [_, setSelectedCookies] = useAtom(atomSelectedCookies);

  const onInfo = (cookie: chrome.cookies.Cookie) => {
    setCookie(JSON.stringify(currentCookie) !== JSON.stringify(cookie) ? cookie : {});
  }

  const onchange = (e: any) => {
    setSelectedCookies(old => e.target.checked ? old + 1 : old - 1)
  }

  const onCopy = (c: chrome.cookies.Cookie) => {
    copy(c)
  }

  const onAdd = (c: chrome.cookies.Cookie) => {
    if (window.confirm(`Do you really want to add "${c.domain}" to blacklist?`)) BlackListService.set(c.domain);
  }

  return <li className={JSON.stringify(currentCookie) === JSON.stringify(c) ? 'box bg-brown' : 'box bg-black'}>
    <div className='d-flex justify-between mb-1'>
      <div className='d-flex'>
        <input type="checkbox" name="cookie" id="cookie" defaultValue={JSON.stringify(c)} onChange={onchange} required />
        <h3 className='m-0 truncate ml-1' title={'(' + index + ') ' + c.name}>{c.name}</h3>
      </div>

      <div className='d-flex align-center'>
        <button className='p-0 bg-transparent ml-1' onClick={() => { onAdd(c) }} title="Add To BlackList"><IconPlus /></button>
        <button className='p-0 bg-transparent mr-1 ml-1' onClick={() => { onInfo(c) }} title="More Info"><IconInfo /></button>
        <button className='p-0 bg-transparent' onClick={() => { onCopy(c) }} title="Copy"><IconCopy /></button>
      </div>
    </div>

    <p className='m-0 gray break-word' title={c.domain}>{c.domain}</p>
  </li>
}