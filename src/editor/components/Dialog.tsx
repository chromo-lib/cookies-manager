import { useAtom } from 'jotai';
import { createPortal } from 'react-dom'
import { atomModal } from '../atoms';

export default function Dialog({ children, title = "" }: any) {
  const [isClosed, setIsClosed] = useAtom(atomModal);

  const onClose = () => {
    setIsClosed(!isClosed)
  }

  return createPortal(<div className={'modal d-flex justify-center align-center ' + (isClosed ? 'd-none' : '')}>
    <div className='modal-content br7'>
      <header className='w-100 d-flex justify-between align-center p-2 border-bottom border-black'>
        <h3 className='m-0'>{title}</h3>
        <button className='p-0' onClick={onClose}>x</button>
      </header>
      <div className='p-2'>{children}</div>
    </div>
  </div>, document.getElementById('modal')!)
}
