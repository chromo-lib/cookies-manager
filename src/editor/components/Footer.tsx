export default function Footer() {
  return <footer className='w-100 bg-dark d-flex align-center justify-between border-top p-1 uppercase gray'>
    <small>🍪 Cookies Manager v{chrome.runtime.getManifest().version}</small>
    <div>
      <small className="mr-1">Created with ❤️ by <a href="https://haikel-fazzani.deno.dev" target="_blank">Haikel Fazzani</a></small>
      <small>📁 <a href="https://gitlab.com/chromo-lib/cookies-manager" target="_blank">Repository</a></small>
    </div>
  </footer>
}