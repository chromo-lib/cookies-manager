import FormSearch from "../forms/FormSearch";
import BtnTheme from "../buttons/BtnTheme";
import BtnLoadMore from "../buttons/BtnLoadMore";
import BtnDeleteCookies from "../buttons/BtnDeleteCookies";
import BtnSelectCookies from "../buttons/BtnSelectCookies";
import BtnAddToBlacklist from "../buttons/BtnAddToBlacklist";
import BtnToggleAside from "../buttons/BtnToggleAside";

export default function Header() {
  return <header className='w-100 bg-dark border-bottom d-flex'>
    <BtnTheme />
    <FormSearch />
    <BtnAddToBlacklist />
    <BtnDeleteCookies />
    <BtnSelectCookies />
    <BtnLoadMore />
    <BtnToggleAside />
  </header>
}