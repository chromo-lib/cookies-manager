import { useState } from "react";
import IconPlus from "../icons/IconPlus";
import BlackListService from "../services/BlackListService";

export default function FormAddToBlacklist() {
  const [domain, setDomain] = useState<string>('');

  const onchange = (e: any) => {
    setDomain(e.target.value.trim());
  }

  const onSubmit = async (e: any) => {
    e.preventDefault();
    try {
      if (domain.startsWith('http')) {
        let domains = [];
        const response = await fetch(domain);
        const data = await response.text();
        const isJSon = response.headers.get("content-type")?.includes('application/json') || data.startsWith('["');
        if (isJSon) domains = JSON.parse(data);
        await BlackListService.setAll(isJSon ? domains : domains.split(/\s+|\r\n/g));
      }
      else await BlackListService.set(domain);

      setDomain('');
    } catch (error) {
      console.log(error);      
    }
  }

  return (
    <div className="border-bottom p-1">
      <p className="m-0 mb-1">Add domain(s) to blacklist</p>
      <form className="d-flex align-center mb-1" onSubmit={onSubmit}>
        <input
          className="w-100 mr-1"
          name="domains"
          id="domains"
          placeholder="example.com"
          onChange={onchange}
          value={domain}
          required></input>
        <button type="submit" className="bg-black" title="Add To Black List"><IconPlus /></button>
      </form>
      <pre className="gray">You have the option to add domains one by one or retrieve them from URL. <br />URL example: https://gitlab.com/-/snippets/2573451</pre>
    </div>
  )
}
