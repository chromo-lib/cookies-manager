import { useAtom } from "jotai"
import { useState } from "react";
import { atomSelectedCookie } from "../atoms"

export default function FormCookieUpdate() {
  const [cookie, setCookie] = useAtom(atomSelectedCookie);
  const [message, setMessage] = useState<string>('')

  if (!cookie || !cookie.domain) return <></>

  const onchange = (e: any) => {
    setCookie({ ...cookie, [e.target.name]: e.target.value });
  }

  const onSubmit = async (e: any) => {
    e.preventDefault();
    try {
      const url = "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
      delete cookie.expirationDate;
      delete cookie.hostOnly;
      delete cookie.session;
      chrome.cookies.set({ url, ...cookie });
      setMessage(`Cookie with name '${cookie.name}' is updated`)
    } catch (error: any) {
      setMessage(error.message)
    }
  }

  return <div className="scale border-bottom p-1">
    <form onSubmit={onSubmit}>
      <div className="d-flex flex-column mb-1">
        <label htmlFor="name">name</label>
        <input className="mt-1" type="text" name="name" onChange={onchange} value={cookie.name} />
      </div>

      <div className="d-flex flex-column mb-1">
        <label htmlFor="domain">domain</label>
        <input className="mt-1" type="text" name="domain" onChange={onchange} value={cookie.domain} />
      </div>

      <div className="d-flex flex-column mb-1">
        <label htmlFor="value">value</label>
        <input className="mt-1" type="text" name="value" onChange={onchange} value={cookie.value} />
      </div>

      <div className="d-flex flex-column mb-1">
        <label htmlFor="path">path</label>
        <input className="mt-1" type="text" name="path" onChange={onchange} value={cookie.path} />
      </div>

      <button className="w-100" type="submit">update</button>
    </form>

    <p>{message}</p>
  </div>
}