import { useAtom } from "jotai";
import { atomBlackListDomains } from "../atoms";
import IconSearch from "../icons/IconSearch";
import BlackListService from "../services/BlackListService";

export default function FormFilterBlacklis() {
  const [_, setDomains] = useAtom(atomBlackListDomains);

  const onSubmit = async (e: any) => {
    e.preventDefault();
    setDomains(await BlackListService.filter(e.target.elements[0].value))
  }

  return <form className="d-flex mb-1" onSubmit={onSubmit}>
    <input className="w-100" type="search" name="query" id="query" placeholder="search.." />
    <button className="bg-dark" type="submit" title="Search By Name"><IconSearch /></button>
  </form>
}
