import IconSearch from "../icons/IconSearch";

export default function FormSearch() {
  const onSubmit = (e: any) => {
    e.preventDefault();
    const query = e.target.elements[0].value.replaceAll(',', '|').replaceAll(/\s+/g, '');
    chrome.runtime.sendMessage({ message: 'get:cookies', query });
  }

  return <>
    <form className='w-100 d-flex align-center' onSubmit={onSubmit}>
      <div className="w-100 pr-1 pl-1 mr-1">
        <input className="w-100 border-0" type="search" name="search" id="search" placeholder='google, facebook, twitter' />
      </div>
      <button className='bg-dark mr-1' type='submit' title="Search by Cookie: name, path and domain"><IconSearch /></button>
    </form>
  </>
}