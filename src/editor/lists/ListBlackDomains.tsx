import { useCallback, useEffect } from 'react'
import { useAtom } from 'jotai';
import { atomBlackListDomains } from '../atoms';
import BlackListService from '../services/BlackListService';
import IconX from '../icons/IconX';

export default function ListBlackDomains() {
  const [domains, setDomains] = useAtom(atomBlackListDomains);

  const onRemove = async (domain: string) => {
    setDomains(await BlackListService.remove(domain))
  }

  const onMessages = useCallback((response: any, sender: any, sendResponse: any) => {
    sendResponse('');
    if (response.blacklist && response.blacklist.length > 0) setDomains(response.blacklist);
  }, []);

  useEffect(() => {
    chrome.runtime.sendMessage({ message: 'get:blacklist' });
    chrome.runtime.onMessage.addListener(onMessages);
    return () => {
      chrome.runtime.onMessage.removeListener(onMessages);
    }
  }, []);

  return <ul className="list bordered mb-1 br7 max-list-h overflow">
    {domains.reverse().map((d, i) => <li className="p-1 d-flex justify-between align-center gray" key={i}>
      <h5 className="truncate m-0" title={d}>{d}</h5>
      <span className="red cp" onClick={() => { onRemove(d) }} title={"Remove '" + d + "' from Blacklist"}><IconX /></span>
    </li>)}
  </ul>
}
