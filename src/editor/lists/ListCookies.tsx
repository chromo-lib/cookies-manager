import { useAtom } from 'jotai'
import { useCallback, useEffect } from 'react'
import { atomCookies, atomCookiesLen } from '../atoms';
import CookieView from '../components/CookieView';

export default function ListCookies({ clx = "" }) {
  const [cookies, setCookies] = useAtom(atomCookies);
  const [_, setCookiesLen] = useAtom(atomCookiesLen);

  const onMessages = useCallback((response: any, sender: any, sendResponse: any) => {
    sendResponse('')
    if (response.cookies && response.cookies.length > 0) {
      setCookies(response.cookies)
      setCookiesLen(response.cookiesLen);
    }
  }, []);

  useEffect(() => {
    chrome.runtime.sendMessage({ message: 'get:cookies' });
    chrome.runtime.onMessage.addListener(onMessages);
    return () => {
      chrome.runtime.onMessage.removeListener(onMessages);
    }
  }, []);

  if (cookies && cookies.length > 0) return <ul className={'h-100 bg-dark overflow p-1 ' + (clx)}>
    {cookies.map((c, i) => <CookieView c={c} index={i} key={i} />)}
  </ul>
  else return <h1 className='d-flex justify-center align-center'>Nothing found, try again.</h1>
}