import { getHost } from "../utils/getHost";

const isChrome = navigator.userAgent.indexOf("Chrome") != -1;

export default class BlackListService {

  static async getAll(): Promise<string[]> {
    const store = await chrome.storage.local.get("blacklist-store");
    return store['blacklist-store'] && store['blacklist-store'].length > 0
      ? store['blacklist-store']
      : []
  }

  static async setAll(domains: string[]): Promise<void> {
    chrome.runtime.sendMessage({ message: 'add:blacklist', domains: domains.map(d => getHost(d)), isChrome });
  }

  static async set(domain: string): Promise<string[]> {
    const olddomains = await this.getAll();
    domain = getHost(domain);

    if (domain && domain.length > 0 && !olddomains.includes(domain)) {
      chrome.runtime.sendMessage({ message: 'add:blacklist', domains: [...olddomains, domain], isChrome });
      return [...olddomains, domain];
    }
    return olddomains
  }

  static async remove(domain: string): Promise<string[]> {
    const domains = await this.getAll();

    if (window.confirm('Do you really want to remove this domains? ' + domain)) {
      const nd = domains.filter(d => domain !== d);
      chrome.storage.local.set({ "blacklist-store": nd });
      return nd;
    }

    return domains;
  }

  static async filter(query: string): Promise<string[]> {
    const domains = await this.getAll();
    return domains.filter((c: string) => c.includes(query))
  }
}