export default function buildurl(cookie: chrome.cookies.Cookie) {
  if (cookie.domain && cookie.domain.startsWith('.')) cookie.domain = 'www' + cookie.domain;
  return "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
}