export default async function copy(data: any) {
  try {
    await navigator.clipboard.writeText(JSON.stringify(data));
  } catch (err) {
    const input = document.createElement('input');
    input.setAttribute('value', JSON.stringify(data));
    document.body.appendChild(input);
    input.select();
    const result = document.execCommand('copy');
    document.body.removeChild(input);
    return result;
  }
}