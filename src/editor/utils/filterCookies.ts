type options = {
  name: string | null,
  secure: boolean | null,
  sameSite: string | null,
  session: boolean | null,
  hostOnly: boolean | null
}

const defaultOptions: options = {
  name: null,
  secure: null,
  sameSite: null,
  session: null,
  hostOnly: null
}

export default async function filterCookies(params?: options) {

  const ops = { ...defaultOptions, ...params };

  const cookies: chrome.cookies.Cookie[] = await chrome.cookies.getAll({});
  let cookiesFiltred: chrome.cookies.Cookie[] = cookies.slice(0);

  cookiesFiltred = cookiesFiltred.filter(ck => ops.name && ops.name.length > 0 ? ck.path.includes(ops.name) : ck);
  cookiesFiltred = cookiesFiltred.filter(ck => ops.secure !== null ? ck.secure === ops.secure : ck);
  cookiesFiltred = cookiesFiltred.filter(ck => ops.session !== null ? ck.session === ops.session : ck);
  cookiesFiltred = cookiesFiltred.filter(ck => ops.hostOnly !== null ? ck.hostOnly === ops.hostOnly : ck);
  cookiesFiltred = cookiesFiltred.filter(ck => ops.sameSite !== null ? ck.sameSite === ops.sameSite : ck);

  return cookiesFiltred.slice(0, 50);
}