export function getHost(domain: string) { // www.example.com => example.com
  domain = domain.startsWith('.') ? domain.slice(1) : domain.replace(/http(s)?:\/\//g, '');

  if (domain.includes('.') && domain.indexOf('.') !== domain.lastIndexOf('.')) {
    return domain.slice(domain.indexOf('.') + 1)
  }
  return domain
}