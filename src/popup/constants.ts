export const btnOpenEditor = document.getElementById('btn-open-editor')! as HTMLButtonElement;
export const btnAddToBlacklist = document.getElementById('btn-add-to-blacklist')! as HTMLButtonElement;
export const tabInfoEL = document.getElementById('tab-info')!;
export const btnRemoveFromBlacklist = document.getElementById('btn-remove-from-blacklist')!;