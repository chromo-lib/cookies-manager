import setMessage from "../utils/setMessage";
import { getHost } from "../utils/getHost";
import { btnAddToBlacklist, btnRemoveFromBlacklist } from "../constants";
import activeTabInfos from "../utils/activeTabInfos";

export default async function onBlock() {
  const activeTab = await activeTabInfos();

  if (activeTab && activeTab.url && window.confirm(`Do you really want to block cookies of this domain "${getHost(activeTab.url)}"?`)) {
    chrome.runtime.sendMessage({
      message: 'add:blacklist',
      isChrome: navigator.userAgent.indexOf("Chrome") != -1,
      activeTab,
      domains: [getHost(activeTab.url)],
      from: 'popup'
    },
      (response) => {
        setMessage(response);

        btnAddToBlacklist.classList.add('d-none');
        btnRemoveFromBlacklist.classList.remove('d-none');
      });
  }
}