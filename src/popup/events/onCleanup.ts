export default async function onCleanup() {
  const cookies = await chrome.cookies.getAll({});
  const store = await chrome.storage.local.get("blacklist-store");
  const blacklist = store['blacklist-store'] && store['blacklist-store'].length > 0 ? store['blacklist-store'] : [];

  chrome.action.setBadgeText({ text: '' + cookies.length });

  chrome.runtime.sendMessage({
    message: 'delete:blacklist-cookies',
    cookies: blacklist,
    isChrome: navigator.userAgent.indexOf("Chrome") != -1
  });
}