import activeTabInfos from "../utils/activeTabInfos";
import setMessage from "../utils/setMessage";

export default async function onDelete() {
  if (window.confirm('Do you really want to delete cookie of the current tab?')) {

    const activeTab = await activeTabInfos();

    if (!activeTab) return;

    chrome.runtime.sendMessage({
      message: 'delete:current-tab-cookies',
      isChrome: navigator.userAgent.indexOf("Chrome") != -1,
      activeTab
    },
      (response) => {
        setMessage(response);
      });
  }

}