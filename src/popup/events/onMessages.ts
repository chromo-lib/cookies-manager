import { btnAddToBlacklist, btnRemoveFromBlacklist, tabInfoEL } from "../constants";
import setMessage from "../utils/setMessage";

export default function onMessages(response: any, _: any, sendResponse: any) {
  sendResponse('');
  if (response.domain) {
    tabInfoEL.querySelector('h2')!.textContent = response.domain;
    tabInfoEL.querySelector('span')!.innerHTML = response.info;
    btnAddToBlacklist.classList.add('d-none');
    btnRemoveFromBlacklist.classList.remove('d-none')
  }

  if(response.message) setMessage(response.message);
}