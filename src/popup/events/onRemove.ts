import { btnAddToBlacklist, btnRemoveFromBlacklist, tabInfoEL } from "../constants";
import activeTabInfos from "../utils/activeTabInfos";
import { getHost } from "../utils/getHost";
import setMessage from "../utils/setMessage";

export default async function onRemove() {
  const activeTab = await activeTabInfos();

  if (activeTab && activeTab.url && window.confirm(`Do you really want to remove this domain from blacklist?" ${getHost(activeTab.url)}"?`)) {
    chrome.runtime.sendMessage({
      message: 'remove:blacklist',
      domain: getHost(activeTab.url),
      from: 'popup'
    },
      (response) => {
        setMessage(response);

        btnAddToBlacklist.classList.remove('d-none');
        btnRemoveFromBlacklist.classList.add('d-none');
        tabInfoEL.querySelector('span')!.innerHTML = '';
      });
  }
}