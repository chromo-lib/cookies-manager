import { btnAddToBlacklist, btnOpenEditor, btnRemoveFromBlacklist, tabInfoEL } from "./constants";
import onBlock from "./events/onBlock";
import onCleanup from "./events/onCleanup";
import onDelete from "./events/onDelete";
import onMessages from "./events/onMessages";
import onOpenEditor from "./events/onOpenEditor";
import onRemove from "./events/onRemove";
import { getHost } from "./utils/getHost";

chrome.tabs.query({ active: true, currentWindow: true }).then(tabs => {
  if (tabs && tabs.length > 0 && tabs[0].url) {
    const url = getHost(tabs[0].url);
    tabInfoEL.querySelector('h2')!.textContent = url;
    tabInfoEL.querySelector('.small')!.textContent = '' + tabs[0].title;
    chrome.runtime.sendMessage({ message: 'check:blacklist', url, isChrome: navigator.userAgent.indexOf("Chrome") != -1 });
  }
});

document.getElementById('btn-cleanup')!.addEventListener('click', onCleanup);
btnOpenEditor.addEventListener('click', onOpenEditor);
btnRemoveFromBlacklist.addEventListener('click', onRemove);
document.getElementById('btn-delete-cookies')?.addEventListener('click', onDelete);
btnAddToBlacklist.addEventListener('click', onBlock);
chrome.runtime.onMessage.addListener(onMessages)