export default async function activeTabInfos(): Promise<chrome.tabs.Tab | null> {
  const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
  const activeTab = tabs[0];
  if (!activeTab || !activeTab.url || !/^(http|ws|chrome|firefox|addon|edge|safari|opera)/g.test(activeTab.url)) return null;
  return tabs[0];
}