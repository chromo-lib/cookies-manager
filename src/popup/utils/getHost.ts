export function getHost(domain: string) { // www.example.com => example.com
  domain = new URL(domain).hostname;

  if (domain.includes('.') && domain.indexOf('.') !== domain.lastIndexOf('.')) {
    return domain.slice(domain.indexOf('.') + 1)
  }
  return domain
}