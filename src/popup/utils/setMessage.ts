export default function setMessage(message: string) {
  let msg = document.getElementById('msg');

  if (!msg) {
    msg = document.createElement('pre')
    msg.id = 'msg';
    document.body.append(msg);
  }

  msg!.innerHTML = message;
}