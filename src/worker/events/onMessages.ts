import handleBlacklist from "../messaging/handleBlacklist";
import handleCookies from "../messaging/handleCookies";

export default function onMessages(request: any, _: any, sendResponse: any) {
  handleBlacklist(request, sendResponse);
  handleCookies(request, sendResponse);
}