import { removeBrowserData, removeCookies } from "../utils";
import sendMessage from "./sendMessage";

export default class CookiesManager {

  static async findMany(url?: string | null | undefined) {
    const cookies = await chrome.cookies.getAll({});
    return url && url.length > 0
      ? cookies.filter(c => {
        const domain = c.domain.startsWith('.') ? c.domain.slice(1) : c.domain;
        return url.includes(domain.replace('www.', ''))
      })
      : cookies
  }

  static deleteMany(cookies: chrome.cookies.Cookie[], isChrome = true) {
    removeCookies(cookies);
    removeBrowserData(cookies, isChrome);
  }

  static async deleteOne(url: string, isChrome = true) {
    const cookies = await this.findMany(url);

    if (cookies && cookies.length > 0) {
      removeCookies(cookies);
      removeBrowserData(cookies, isChrome);
      return true
    }

    sendMessage({ message: 'No cookies are stored' });
  }
}