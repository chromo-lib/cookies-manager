import onMessages from "./events/onMessages";

chrome.runtime.setUninstallURL('https://haikel-fazzani.deno.dev');
chrome.runtime.onMessage.addListener(onMessages);