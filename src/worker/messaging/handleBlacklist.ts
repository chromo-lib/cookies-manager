import sendMessage from "../helpers/sendMessage";
import BlacklistStorage from "../storage/BlacklistStorage";
import { removeBrowserData } from "../utils";

export default function handleBlacklist(request: any, sendResponse: any) {

  const { message, cookies, domains, url, isChrome } = request;

  if (message === 'check:blacklist') {
    sendResponse('check:blacklist');
    BlacklistStorage.findOne(url).then(domain => {
      sendMessage({ domain, info: domain ? `<span class="red">Blacklisted</span>` : '' });
    });
  }

  if (message === 'get:blacklist') {
    sendResponse('get:blacklist');
    BlacklistStorage.findMany().then(blacklist => {
      sendMessage({ blacklist });
    });
  }

  if (message === 'add:blacklist' && domains.length > 0) {
    sendResponse('add:blacklist');
    BlacklistStorage.add(domains).then(blacklist => {
      sendMessage({ blacklist, message: request.from === 'popup' ? `<span class="red">${domains[0]}</span> is added to blacklist` : '' });
    });
  }

  if (message === 'remove:blacklist') {
    sendResponse('remove:blacklist');
    BlacklistStorage.deleteOne(request.domain).then(isRemoved => {
      sendMessage({ message: `<span class="red">${request.domain}</span> is removed from blacklist` });
    });
  }

  if (message === 'delete:blacklist-cookies' && cookies.length > 0) {
    sendResponse({ chunkLen: 50 });
    BlacklistStorage.deleteMany(cookies).then(blacklist => {
      sendMessage({ blacklist });
      removeBrowserData(cookies, isChrome);
    });
  }

  if (message === 'clear:blacklist') {
    sendResponse([]);
    BlacklistStorage.clear();
  }

  return true;
}