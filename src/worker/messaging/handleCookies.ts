import CookiesManager from "../helpers/CookiesManager";
import sendMessage from "../helpers/sendMessage";

export default function handleCookies(request: any, sendResponse: any) {

  const { message, query, chunkLen, cookies, activeTab, isChrome } = request;

  if (message === 'get:cookies') {
    sendResponse({ chunkLen: chunkLen });

    CookiesManager.findMany().then(cks => {
      let cookies = cks.slice(0);

      if (query) {
        const regex = new RegExp(query, 'gi');
        cookies = cks.filter(c => (regex.test(c.name) || regex.test(c.domain) || regex.test(c.path)));
      }

      sendMessage({ cookies: cookies.slice(0, chunkLen || 50), cookiesLen: cks.length });
    });
  }

  if (message === 'delete:cookies' && cookies.length > 0) {
    sendResponse({ chunkLen: 50 });
    CookiesManager.deleteMany(cookies, isChrome);
  }

  if (message === 'delete:current-tab-cookies' && activeTab) {
    sendResponse(`Start processing: deleting cookies of '${activeTab.title}'`);
    CookiesManager.deleteOne(activeTab.url, isChrome);
  }

  return true;
}