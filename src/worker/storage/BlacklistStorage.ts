import { getHost, isArrayOfStrings, removeCookies } from "../utils";

export default class BlacklistStorage {

  private static key = "blacklist-store";

  static async findMany(): Promise<string[]> {
    const store: any = await chrome.storage.local.get("blacklist-store");
    return store[this.key] && store[this.key].length > 0 ? store[this.key] : [];
  }

  static async deleteMany(data: chrome.cookies.Cookie[] | string[]) {
    const cookies = isArrayOfStrings(data) ? await chrome.cookies.getAll({}) : data;
    let foundedCookies: chrome.cookies.Cookie[] = [];

    if (isArrayOfStrings(data)) foundedCookies = cookies.filter(c => data.includes(getHost(c.domain)));

    removeCookies(foundedCookies);
    return await this.findMany();
  }

  static async add(domains: string[]) {
    const blacklist = await this.findMany();

    domains.forEach((v: string) => {
      if (!blacklist.includes(v)) blacklist.push(v);
      return v
    });

    await chrome.storage.local.set({ [this.key]: blacklist });
    return blacklist;
  }

  static async findOne(url: string): Promise<string | undefined> {
    const data = await this.findMany();
    return data.find(d => d === url);
  }

  static async deleteOne(domain: string): Promise<boolean> {
    const domains = await this.findMany();
    const nd = domains.filter(d => domain !== d);
    chrome.storage.local.set({ [this.key]: nd });
    return true
  }

  static clear() {
    chrome.storage.local.remove(this.key);
  }
}