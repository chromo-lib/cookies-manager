import sendMessage from "./helpers/sendMessage";

export function isArrayOfStrings(value: unknown): value is string[] {
  return Array.isArray(value) && value.every(item => typeof item === "string");
}

export function getHost(domain: string) { // www.example.com => example.com
  domain = domain.startsWith('.') ? domain.slice(1) : domain;

  if (domain.includes('.') && domain.indexOf('.') !== domain.lastIndexOf('.')) {
    return domain.slice(domain.indexOf('.') + 1)
  }
  return domain
}

export function buildurl(cookie: chrome.cookies.Cookie) {
  if (cookie.domain && cookie.domain.startsWith('.')) cookie.domain = 'www' + cookie.domain;
  return "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
}

export function removeCookies(cookies: chrome.cookies.Cookie[]) {
  if (cookies.length > 0) cookies.forEach((cookie: chrome.cookies.Cookie) => chrome.cookies.remove({ name: cookie.name, url: buildurl(cookie), storeId: cookie.storeId }));

  chrome.cookies.getAll({}, (cks: chrome.cookies.Cookie[]) => {
    chrome.action.setBadgeText({ text: '' + cks.length });

    sendMessage({
      cookies: cks.slice(0, 50),
      cookiesLen: cks.length,
      message: cookies.length > 0 ? `<small class="gray mb-1">Deleted Cookies (${cookies.length})</small><br />`
        + cookies.map(c => `<span>[${c.domain}] ${c.name}</span>`).join('\n')
        : 'Clean'
    });
  });
}

export function removeBrowserData(data: chrome.cookies.Cookie[] | string[], isChrome: boolean) {
  const hostnames: string[] = [];

  let dataTypeSet = isChrome
    ? { cache: true, localStorage: true, cacheStorage: true, serviceWorkers: true, indexedDB: true, cookies: true, appcache: true, webSQL: true, fileSystems: true }
    : { localStorage: true, serviceWorkers: true, indexedDB: true, cookies: true };

  data.forEach((cookieOrDomain: string | chrome.cookies.Cookie) => {
    cookieOrDomain = typeof cookieOrDomain === 'string' ? cookieOrDomain : getHost(cookieOrDomain.domain);
    if (!hostnames.includes(cookieOrDomain)) hostnames.push(isChrome ? 'https://' + cookieOrDomain : cookieOrDomain);
  });

  chrome.browsingData.remove({ [isChrome ? "origins" : "hostnames"]: hostnames }, dataTypeSet);
}