import { parse, resolve } from 'path';
import { defineConfig, UserConfigExport } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  root: 'src',
  publicDir: 'static',
  build: {
    rollupOptions: {
      input: {
        worker: resolve(__dirname, 'src', 'worker', 'index.ts'),
        editor: resolve(__dirname, 'src','editor.html'),
        popup: resolve(__dirname, 'src', 'popup', 'index.ts'),
      },
      output: {
        dir: "dist",
        chunkFileNames: "[name].[hash].js",
        entryFileNames: "[name].js",
        assetFileNames: (assetInfo) => {
          const { name } = parse(assetInfo.name);
          return `${name}.[ext]`;
        },
      },
    }
  },
  plugins: [
    react()
  ]
} as UserConfigExport);